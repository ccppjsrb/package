use strict;
use warnings;
use utf8;

my @test = (
    "y1 t1  3   0",    
    "y2 t2  3   1",    
    "y3 t3  3   0",    
    "y4 t4  3   1",    
    "y5 t5  3   0",    
    "y6 t6  3   1",    
    "y7 t7  3   2",    
);
my @rel = rel_lines(@test);

print rel_cmp($rel[0], $rel[1]);
sub rel_lines{
    my @lines = @_;
    my @result = ();
    my $ref = [];
    
    foreach my $line(reverse @lines){
        next if $line =~ /^\s*;/;
        unshift @{$ref}, $line;
        my @sp = split(/\s+/, $line);
        if($sp[3] eq "0"){
            unshift @result, $ref;
            $ref = [];
        }
    }
    return @result;
}
sub rel_cmp{
    my ($ref_a, $ref_b) = @_;
    
    if(scalar(@{$ref_a}) <=> scalar(@{$ref_b})){
        return scalar(@{$ref_a}) <=> scalar(@{$ref_b});
    }
    else{
        while( my ($i, $val) = each @{$ref_a}){
            if($ref_a->[$i] cmp $ref_b->[$i]){
                return $ref_a->[$i] cmp $ref_b->[$i];
            }
        }
    }
    return 0;
}
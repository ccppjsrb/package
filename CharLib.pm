package CharLib;
use strict;
use warnings;
use utf8;
use Encode qw(encode decode);

sub new{
	my $class = shift;
	my $self = {
		@_
	};
	return bless $self, $class;
}

sub byte_to_unicode{
	my ($class, $bytes) = @_;
	
	$bytes =~ s/\\W//g;
	my $charset = "UTF-16BE";
	my $packed = pack("H*", $bytes);
	my $decoded = decode($charset, $packed);
	my @codes = ();
	foreach my $c(split(//, $decoded)){
		my $encoded = encode($charset, $c);
		my $unpacked = unpack("H*", $encoded);
		my @matched = $unpacked =~ /(d[8-9a-b][0-9a-f]{2})(d[c-f][0-9a-f]{2})/;
		
		if(@matched){
			my ($upper, $lower) = @matched;
			my $quotient = hex($upper) - hex("d800");
			my $remainder = hex($lower) - hex("dc00");
			my $num = hex("10000") + hex("400") * $quotient + $remainder;
			push @codes, sprintf("%04x", $num);
		}
		else{
		    push @codes, $unpacked;
		}
	}
	return uc join("", map{ "U+" . $_ } @codes);
}

sub unicode_to_byte{
    my ($class, $unicode) = @_;
    my $charset = "UTF-16BE";
    my @bytes = ();
    $unicode = lc $unicode;
    my @unicodes = $unicode =~ /u\+([0-9a-f]+)/g;
    foreach my $code(@unicodes){
        if($code =~ /^[0-9a-f]{4}$/){
            push @bytes, $code;
        }
        elsif($code =~ /^(10|[1-9a-f])[0-9a-f]{4}$/){
			my $num = hex($code) - hex("10000");
			my $quotient = int($num / hex("400"));
			my $remainder = $num % hex("400");
			push @bytes, sprintf("%04x", hex("d800") + $quotient);            
			push @bytes, sprintf("%04x", hex("dc00") + $remainder);            
        }
    }
    return uc join("", map{ '\X' . $_ } @bytes);
}

1;
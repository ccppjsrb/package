<<<<<<< HEAD
use strict;
use warnings;
use utf8;
use FindBin;
use lib "$FindBin::Bin/..";
use UtilLib;
use File::Spec qw(catfile);
use File::Path qw(mkpath);
use Encode qw(encode decode);
use File::Basename qw(basename);

my $level = shift;
my $charset = "SJIS";
my $file_charset = "UTF-8";
my $util_lib = UtilLib->new();
#レベルごとに許容される漢字一覧を作成
my @files = $util_lib->rec_opendir(File::Spec->catfile($FindBin::Bin, "level", $level));
my @valids = ();
foreach my $file(@files){
	my @lines = $util_lib->openfile($file, $file_charset);
	foreach my $line(@lines){
		next if $line =~ /^\s*(;|$)/;
		my @sp = split(/\s+/, $line);
		push @valids, $sp[0];
	}
}
my $valid = join("", @valids);
#入力ファイルをすべて検索
my @in_files = $util_lib->rec_opendir("in");
foreach my $in_file(@in_files){
	my @in_lines = $util_lib->openfile($in_file, $file_charset);
	my @out_lines = ();
	foreach my $in_line(@in_lines){
		#コメント行はそのまま出力
		if($in_line =~ /^\s*(;|$)/){
			push @out_lines, $in_line;
			next;
		}
		#表記を対象
		my @sp = split(/\s+/, $in_line);
		my $exp = $sp[1];
		my @result = ();
		#1文字ずつ○×で出力
		foreach my $ch(split(//, $exp)){
			if($valid =~ /$ch/){
				push @result, "○";
			}
			else{
				push @result, "×";
			}
		}
		push @out_lines, sprintf("%s\t%s", join("", @result), $in_line);
	}
	my $out_file = decode($charset, $in_file);
	$out_file =~ s/^in/out/;
	$out_file = encode($charset, $out_file);
	mkpath(basename($out_file));
	$util_lib->writefile($out_file, $file_charset, \@out_lines);
=======
use strict;
use warnings;
use utf8;
use FindBin;
use lib "$FindBin::Bin/..";
use UtilLib;
use File::Spec qw(catfile);
use File::Path qw(mkpath);
use Encode qw(encode decode);
use File::Basename qw(basename);

my $level = shift;
my $charset = "SJIS";
my $file_charset = "UTF-8";
my $util_lib = UtilLib->new();
#レベルごとに許容される漢字一覧を作成
my @files = $util_lib->rec_opendir(File::Spec->catfile($FindBin::Bin, "level", $level));
my @valids = ();
foreach my $file(@files){
	my @lines = $util_lib->openfile($file, $file_charset);
	foreach my $line(@lines){
		next if $line =~ /^\s*(;|$)/;
		my @sp = split(/\s+/, $line);
		push @valids, $sp[0];
	}
}
my $valid = join("", @valids);
#入力ファイルをすべて検索
my @in_files = $util_lib->rec_opendir("in");
foreach my $in_file(@in_files){
	my @in_lines = $util_lib->openfile($in_file, $file_charset);
	my @out_lines = ();
	foreach my $in_line(@in_lines){
		#コメント行はそのまま出力
		if($in_line =~ /^\s*(;|$)/){
			push @out_lines, $in_line;
			next;
		}
		#表記を対象
		my @sp = split(/\s+/, $in_line);
		my $exp = $sp[1];
		my @result = ();
		#1文字ずつ○×で出力
		foreach my $ch(split(//, $exp)){
			if($valid =~ /$ch/){
				push @result, "○";
			}
			else{
				push @result, "×";
			}
		}
		push @out_lines, sprintf("%s\t%s", join("", @result), $in_line);
	}
	my $out_file = decode($charset, $in_file);
	$out_file =~ s/^in/out/;
	$out_file = encode($charset, $out_file);
	mkpath(basename($out_file));
	$util_lib->writefile($out_file, $file_charset, \@out_lines);
>>>>>>> 2d67f121c750e045303ad59d19d91205eeeffd62
}
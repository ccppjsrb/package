use strict;
use warnings;
use utf8;
use FindBin;
use lib "$FindBin::Bin/..";
use UtilLib;
use Encode qw(encode decode);

if(scalar(@ARGV) != 2){
	die qq(usage: perl grep.pl dir word);
}
my $dir = shift;
my $word = shift;
my $file_charset = "SJIS";
my $charset = "UTF-8";
$word = decode($charset, $word);

my $util_lib = UtilLib->new();
my @files = $util_lib->rec_opendir($dir);
foreach my $file(@files){
	my @lines = $util_lib->openfile($file, $file_charset);
	$file = decode($charset, $file);
	while( my($i, $line) = each @lines){
		if($line =~ /$word/){
			print encode($charset, "${file}(${i}):${line}\n");
		}
	}
}

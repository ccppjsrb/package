use strict;
use warnings;
use utf8;
use FindBin;
use lib "$FindBin::Bin/..";
use UtilLib;
use Encode qw(encode decode);

my @dirs = qw(UTF-8 UTF16-BE SJIS EUC-JP);
my $util_lib = UtilLib->new();
foreach my $dir(@dirs){
    my @files = $util_lib->rec_opendir($dir);
    foreach my $file(@files){
    	my @lines = $util_lib->openfile($file, $dir);
    	@lines = grep { $_ !~ /^\s*(;|$)/} @lines;
    	my @sorted_lines = sort { $a =~ s/(.+?\t.+?)\t.+$/$1/r cmp $b =~ s/(.+?\t.+?)\t.+$/$1/r } @lines;
    	$util_lib->writefile($file, $dir, \@sorted_lines);
    }
}
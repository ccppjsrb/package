package StringConverter;
use strict;
use warnings;
use utf8;
use Encode qw(encode decode);
use File::Spec;

sub new{
	my $class = shift;
	my $self = {
		charset => "utf8",
		dic => {},
		@_
	};
	binmode(STDERR, ":encoding(cp932)");
	return bless $self, $class;
}
sub make_dic{
	my ($self, $ux_path) = @_;

	open(my $fh, "<", $ux_path) or die(qq/Can't open file $ux_path/);
	binmode($fh, ":encoding($self->{charset})");
	while(my $line = <$fh>){
		chomp($line);
		#コメント行は飛ばす
		next if $line =~ /^\s*(;|$)/;
		my @sp = split(/\t/, $line);
		if(!exists($self->{dic}->{$sp[1]})){
			$self->{dic}->{$sp[1]} = [];
		}
		#表記 => [{yomi => 読み, freq => 頻度}] の形式で作成する。同じ読みは1つしか入れない
		push @{$self->{dic}->{$sp[1]}}, { yomi => $sp[0], freq => $sp[3] } if !grep { $_->{yomi} eq $sp[0] } @{$self->{dic}->{$sp[1]}};
	}
	#読み候補を頻度順で並べる
	foreach my $key(keys %{$self->{dic}}){
		@{$self->{dic}->{$key}} = sort { $a->{freq} <=> $b->{freq} } @{$self->{dic}->{$key}};
	}
	close($fh);
}

sub split_word{
	my ($self, $reading, $expr) = @_;

	#分割の対象は漢字を含み表記が2文字以上の語彙のみ
	return ([$reading], [$expr]) unless $expr =~ /\p{Han}/ and length($expr) > 1;

	my @stack = ({
		reading => $reading,
		expr => $expr,
		sp_reading => [],
		sp_expr => [],
	});

	while(scalar(@stack) > 0){
		#保存された読みと表記を取り出す
		my $data = pop(@stack);
		if($data->{reading} eq "" and $data->{expr} eq ""){
			#最後まで分割できてたら結果を返す
			return ($data->{sp_reading}, $data->{sp_expr});
		}
		#表記の先頭部分がひらがなとカタカナのみならひらがなに変換したものが読みの先頭と一致するかどうかチェック
		if($data->{expr} =~ /^([ぁ-ヶー]+)/){
			my @hiragana_readings = ();
			my $hiragana_expr = $1;
			my $hiragana = $1;
			$hiragana =~ tr/ァ-ンヵ/ぁ-んか/;
			#ヶの読みは2つあるので2つとも登録
			if($hiragana =~ /ヶ/){
				push @hiragana_readings, $hiragana =~ s/ヶ/か/r;
				push @hiragana_readings, $hiragana =~ s/ヶ/が/r;
			}
			else{
				push @hiragana_readings, $hiragana;
			}
			#ヴを含むものはバ行に変換したものも登録。ヶの読みが2通りあるとどっちに対しても変換したものを作成するので、最大で読み候補は4通りになる
			if($hiragana =~ /ヴ/){
				push @hiragana_readings, map {
					$_ =~ s/ヴぁ/ば/g;
					$_ =~ s/ヴぃ/び/g;
					$_ =~ s/ヴぇ/べ/g;
					$_ =~ s/ヴぉ/ぼ/g;
					$_ =~ s/ヴ/ぶ/g;
					$_
				} @hiragana_readings;
			}
			#ひらがな部分の読み候補をループ
			foreach my $hiragana_reading(@hiragana_readings){
				if($data->{reading} =~ /^\Q$hiragana_reading\E/){
					push @stack, $self->make_new_data($data, [$hiragana_reading], [$hiragana_expr]);
					last;
				}
			}
			next;
		}
		elsif($data->{expr} =~ /\p{Han}/){
			#マッチした部分より前の部分
			#@-はマッチしたものの開始位置
			#@+はマッチしたものの終了位置
			my $prev_expr = substr($data->{expr}, 0, $-[0]);
			#表記の候補として1文字から明日・明後日のように複数文字で1つの読みとなる可能性があるので
			#漢字の先頭からの部分文字列をすべて候補にする
			my @candidates_expr = map { substr($data->{expr}, $-[0], $_) } (1..(length($data->{expr}) - $-[0] + 1));
			foreach my $candidate_expr(@candidates_expr){
				my @candidate_readings = ();
				if($candidate_expr =~ /^[〃仝々]$/){
					my $prev_reading = pop(@{$data->{sp_reading}});
					my $prev_expr = pop(@{$data->{sp_expr}});
					#繰り返し文字のときは前の読みと1セットにするため確定した1つ前の読みと表記を戻す
					$data->{reading} = $prev_reading . $data->{reading};
					$data->{expr} = $prev_expr . $data->{expr};
					#前の読みの繰り返しを読み候補にする
					push @candidate_readings, $prev_reading . $prev_reading;
					my $first_case = substr($prev_reading, 0, 1);
					#繰り返しの読みは頭文字が濁ることが多いので頭文字を濁点に変換して読み候補にする
					$first_case =~ tr/かきくけこ/がぎぐげご/;
					$first_case =~ tr/さしすせそ/ざじずぜぞ/;
					$first_case =~ tr/たちつてと/だぢづでど/;
					$first_case =~ tr/はひふへほ/ばびぶべぼ/;
					push @candidate_readings, $prev_reading . $first_case . substr($prev_reading, 1);
					#辞書に登録があればそれも読み候補にする
					if(exists($self->{dic}->{$candidate_expr})){
						push @candidate_readings, map { $prev_reading . $_->{yomi} } @{$self->{dic}->{$candidate_expr}};
					}
					#前の表記と1セットにする
					$candidate_expr = $prev_expr . $candidate_expr;
				}
				#辞書に表記の登録があれば読み候補にする
				elsif(exists($self->{dic}->{$candidate_expr})){ 
					@candidate_readings = map { $_->{yomi} } @{$self->{dic}->{$candidate_expr}};
				}
				#読み候補を順にループする
				foreach my $candidate_reading(@candidate_readings){
					my @new_datas = ();
					
					while($data->{reading} =~ /\Q$candidate_reading\E/g){
						#マッチした部分の前の読み
						my $prev_reading = substr($data->{reading}, 0, $-[0]);
						#マッチした漢字が先頭だった場合は候補のみ入れる
						if($prev_expr eq ""){
							push @new_datas, $self->make_new_data($data, [$candidate_reading], [$candidate_expr]);
						}
						#マッチした部分の前に表記があった場合それまでの読み・表記も入れる
						else{
							push @new_datas, $self->make_new_data($data, [$prev_reading, $candidate_reading], [$prev_expr, $candidate_expr]);							
						}
					}
					#先頭に近いマッチほど優先したいので@new_datasを逆順にして追加
					push @stack, reverse @new_datas;
				}
			}
		}
		else{
			#ひらがな・カタカナ以外の文字で始まり漢字を含んでなければ残りは分割なしなのですべて1まとまり
			push @stack, $self->make_new_data($data, [$data->{reading}], [$data->{expr}]);
		}

	}
	print STDERR "$reading\t$expr\n";
	return ([$reading], [$expr]);
}

sub make_new_data{
	my ($self, $data, $candidate_readings, $candidate_exprs) = @_;
	my $new_data = {
		reading => $data->{reading},
		expr => $data->{expr},
		sp_reading => [],
		sp_expr => [],
	};
	#読み・表記の候補を置換する
	my $rep_reading = join("", @{$candidate_readings});
	my $rep_expr = join("", @{$candidate_exprs});
	$new_data->{reading} =~ s/^\Q$rep_reading\E//;
	$new_data->{expr} =~ s/^\Q$rep_expr\E//;
	#分割された読みと表記の配列を作成
	push @{$new_data->{sp_reading}}, @{$data->{sp_reading}}, @{$candidate_readings};
	push @{$new_data->{sp_expr}}, @{$data->{sp_expr}}, @{$candidate_exprs};
	return $new_data;

}
1;